//
//  ViewController.swift
//  hackathonW18
//
//  Created by Nathan Geronimo on 3/23/18.
//  Copyright © 2018 Nathan Geronimo. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    let configuration = ARWorldTrackingConfiguration()
    var musicPlaying = false
    
    var greenhill = AVAudioPlayer()
    var ring = AVAudioPlayer()
    var loss = AVAudioPlayer()
    var way = AVAudioPlayer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        configuration.planeDetection = .horizontal
        
//        self.sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints /*, ARSCNDebugOptions.showWorldOrigin */]
        self.sceneView.session.run(configuration)
        self.sceneView.autoenablesDefaultLighting = true
    
        
        let musicPath = Bundle.main.path(forResource: "Green_Hill_Zone.mp3", ofType: nil)!
        let musicUrl = URL(fileURLWithPath: musicPath)
        let soundPath = Bundle.main.path(forResource: "Ring_Sound.mp3", ofType: nil)!
        let soundUrl = URL(fileURLWithPath: soundPath)
        let lossPath = Bundle.main.path(forResource: "Ring_Loss.mp3", ofType: nil)!
        let lossUrl = URL(fileURLWithPath: lossPath)
        let wayPath = Bundle.main.path(forResource: "Way.mp3", ofType: nil)!
        let wayUrl = URL(fileURLWithPath: wayPath)

        do {
            greenhill = try AVAudioPlayer(contentsOf: musicUrl)
            greenhill.volume = 0.3
            ring = try AVAudioPlayer(contentsOf: soundUrl)
            ring.volume = 1.0

            loss = try AVAudioPlayer(contentsOf: lossUrl)
            loss.volume = 0.2

            way = try AVAudioPlayer(contentsOf: wayUrl)
            way.volume = 1.0
        } catch {
            
        }

        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        // Create a new scene
//        let scene = SCNScene(named: "art.scnassets/ship.scn")!
        let scene = SCNScene()

        
        // Set the scene to the view
        sceneView.scene = scene
        
        // Hide the bottom debug menu
        sceneView.showsStatistics = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()


        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    // MARK: - ARSCNViewDelegate
    

    @IBAction func add(_ sender: Any) {
        if (!musicPlaying) {
            musicPlaying = true
            greenhill.play()
        }
        
        ring.play()
        for _ in 1...5 {
            drawShapes()
        }

        
//        node.geometry = SCNBox(width: 0.1, height: 0.1, length: 0.1, chamferRadius: 0)
//        node3.geometry = SCNSphere(radius: 0.05)

    }
    
    func drawShapes() {
        let node = SCNNode()
        
        node.geometry = SCNTorus(ringRadius: 0.05, pipeRadius: 0.01)
        node.geometry?.firstMaterial?.specular.contents = UIColor.white
        node.geometry?.firstMaterial?.diffuse.contents = UIColor.yellow
        let x = randomNumbers(firstNum: -4.0, secondNum: 4.0)
        let y = randomNumbers(firstNum: -1.0, secondNum: 1.0)
        let z = randomNumbers(firstNum: -4.0, secondNum: -1.0)
        node.position = SCNVector3(x,y,z)
        node.runAction(SCNAction.rotateBy(x: 1.6, y: 0.0, z: 0.0, duration: 0))
        node.runAction(SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: 1, z: 0, duration: 2)))
        
        
//        let spin = CABasicAnimation(keyPath: "rotation")
        // Use from-to to explicitly make a full rotation around z
//        spin.duration = 3
//        spin.repeatCount = .infinity
//        node.addAnimation(spin, forKey: "rotation")

        self.sceneView.scene.rootNode.addChildNode(node)
    }
    
//    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
//        guard let anchorPlane = anchor as? ARPlaneAnchor else { return }
//        print ("New Plane found at: ", anchorPlane.extent)
//    }
//
//    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
//        guard let anchorPlane = anchor as? ARPlaneAnchor else { return }
//        print ("New Plane updated at: ", anchorPlane.extent)
//    }
    
    @IBAction func clear(_ sender: Any) {
        self.sceneView.session.pause()
        loss.play()
        greenhill.stop()
        musicPlaying = false
        self.sceneView.scene.rootNode.enumerateChildNodes { (node, _) in
            node.removeFromParentNode()
        }
        
        self.sceneView.session.run(configuration, options: [.removeExistingAnchors, .resetTracking]) // Forgets about old position and gets a new position

    }
    
    @IBAction func reset(_ sender: Any) {
        self.restartSession()
    }
    
    func restartSession() {
        self.sceneView.session.pause()
//        self.sceneView.scene.rootNode.enumerateChildNodes { (node, _) in
//            node.removeFromParentNode()
//        }
        self.sceneView.session.run(configuration, options: [.removeExistingAnchors, .resetTracking]) // Forgets about old position and gets a new position
        way.play()
    }
    
    func randomNumbers(firstNum: CGFloat, secondNum: CGFloat) -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UINT32_MAX) * abs(firstNum - secondNum) + min(firstNum,secondNum)
    }
    
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
